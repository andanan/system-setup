#!/bin/bash
# utility functions for modules

git_install() {
	# Usage: git_install URL TARGET_DIR [BRANCH]
	local repo_url="$1"
	local target_dir="$2"
	local branch="$3"
	require_commands_exist 'git'
	if [[ -d $target_dir/.git ]]; then
		(
			cd "$target_dir"
			local head_ref=$(git symbolic-ref -q HEAD)
			local remote_branch=$(git 'for-each-ref' --format='%(upstream:short)' "$head_ref")
			local remote="${remote_branch%%/*}"
			remote_branch="${remote_branch#*/}"
			local remote_url=$(git remote get-url "$remote")
			if [[ $remote_url == "$repo_url" ]]; then
				if [[ -z $branch || $remote_branch == "$branch" ]]; then
					exec_cmd git pull
				else
					log ERROR - <<- EOF
						Wrong remote branch configured!
						  expected: $branch
						  but was:  $remote_branch
					EOF
					return 1
				fi
			else
				log ERROR - <<- EOF
					Wrong remote configured!
					  expected: $repo_url
					  but was:  $remote_url
				EOF
				return 1
			fi
		)
	else
		local git_args=("$repo_url" "$target_dir")
		if [[ -n "$branch" ]]; then
			git_args=(--single-branch --branch "$branch" "${git_args[@]}")
		fi
		exec_cmd git clone "${git_args[@]}"
	fi
}

# usage: exec_if (command|file|dir) ARG (exists|is_missing) CALLBACK [ARG...]
#        exec_if scope SCOPE is_active CALLBACK [ARG...]
exec_if() {
	local mode="$1"
	local arg="$2"
	local state="$3"
	local callback="$4"
	shift 4
	local exec_callback='false'
	case $mode in
		command|file|dir)
			case $state in
				exists)
					if "_does_${mode}_exist" "$arg"; then
						exec_callback='true'
					fi
				;;
				is_missing)
					if ! "_does_${mode}_exist" "$arg"; then
						exec_callback='true'
					fi
				;;
				*)
					log ERROR "Invalid state: $state"
					log ERROR 'Expected one of: (exists|is_missing)'
					return 1
				;;
			esac
		;;
		scope)
			case $state in
				is_active)
					if is_scope_active "$arg"; then
						exec_callback='true'
					fi
				;;
				*)
					log ERROR "Invalid state: $state"
					log ERROR 'Expected one of: (is_active)'
					return 1
				;;
			esac
		;;
		*)
			log ERROR 'Mode not found: $mode'
			log ERROR 'Expected one of: (command|file|dir)'
			return 1
		;;
	esac
	if [[ $exec_callback == 'true' ]] && command -v "$callback" &>/dev/null; then
		if [[ $# == 0 ]]; then
			"$callback" "$arg"
		else
			"$callback" "$@"
		fi
	fi
}
_does_command_exist() {
	command -v "$1" &>/dev/null
}
_does_file_exist() {
	[[ -f $1 ]]
}
_does_dir_exist() {
	[[ -d $1 ]]
}
require_commands_exist() {
	local cmd
	for cmd in "$@"; do
		if ! _does_command_exist "$cmd"; then
			log ERROR "Command not found: $cmd"
			return 1
		fi
	done
	return 0
}

mark_output() {
	# Usage: mark_output [PREFIX [COLOR]]
	local ph=' |'
	sed "s/^/$ph/"
}

exec_cmd() {
	# execute command and prefix its output:
	"$@" |& mark_output
}

create_file() {
	# TODO: streamline arguments. Consider own impl rather than install wrapper
	# Usage: create_file TARGET_FILE -
	#        create_file <args for `install` command>
	local target_file="$1"
	if [[ $# == 1 || $2 == '-' ]]; then
		install_args=( <(cat) "$target_file" )
	elif [[ $# -gt 1 ]]; then
		install_args=( "$@" )
	else
		log ERROR <<- EOF
			Usage: create_file  TARGET_PATH  -
			       create_file  <args for \`install\` command>
		EOF
		return 1
	fi
	if install "${install_args[@]}" && [[ -f $target_file ]]; then
		echo "Created file: $target_file"
		mark_output < "$target_file"
	else
		log ERROR "Failed to create file: $target_file"
		return 1
	fi
}

download_resource() {
	# Usage: download_resource RESOURCE [TARGET_PATH|-]
	local url="$1"
	local target_path="${2:--}"
	local cmd=()
	if command -v curl &>/dev/null; then
		cmd=(curl -s)
	elif command -v wget &>/dev/null; then
		cmd=(wget -q -O -)
	else
		log WARN "download_resource: No downloader found! Install either curl or wget!"
		return 1
	fi
	if [[ $target_path == '-' ]]; then
		"${cmd[@]}" "$url" 2>/dev/null
	else
		create_file "$target_path" < <("${cmd[@]}" "$url" 2>/dev/null)
	fi
}

if ! (return 0 &>/dev/null); then
	log TRACE "! ${BASH_SOURCE[0]}"
else
	log TRACE ". ${BASH_SOURCE[0]}"
fi

