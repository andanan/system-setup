#!/bin/bash
# functions only! (no direct functionality)

_parse_config_file() {
	local SEPARATOR=':'
	local file="$1"
	local sed_opts=(
		-e 's/^[[:space:]]*//'  # remove leading whitespace
		-e 's/[[:space:]]*$//'  # remove trailing whitespace
		-e '/^$/d'              # remove empty lines
		-e '/^#/d'              # remove comment lines
	)
	local awt_script='
		BEGIN {
			section = "default"
			prefix = section separator
			array_key = ""
		}

		/^\[.*\]$/ {
			# extract section name
			section = substr($0, 2, length($0) - 2)
			prefix = section separator
			next
		}

		/^\)$/ {
			# handle end of multi-line array
			array_key = ""
			next
		}

		/^[^=+]+\+?=\($/ {
			# handle start of multi-line array
			array_key = substr($0, 1, length($0) - 2)
			last_char = substr(array_key, length(array_key))
			if (last_char == "+") {
				# append (do not print empty append)
				# strip-off trailing +
				array_key = substr(array_key, 1, length(array_key) - 1)
			} else {
				# set/assign
				print prefix $0 ")"
			}
			next
		}

		array_key == "" {
			# prepend section name to each line that is not part of a multi-line array
			print prefix $0
		}

		array_key != "" {
			# handle lines that are part of a multi-line array
			print prefix array_key "+=(" $0 ")"
		}
	'
	while read -r line; do
		log TRACE "line: $line"
		if [[ $line =~ ^[[:alnum:]._-]+ ]]; then
			local section="${BASH_REMATCH[0]}"
			line="${line#$section$SEPARATOR}"
		else
			log WARN "Malformed section: $line"
			continue
		fi
		if [[ $line =~ ^[[:alnum:]._-]+ ]]; then
			local key="${BASH_REMATCH[0]}"
			line="${line#$key}"
		else
			log WARN "Malformed key: $line"
			continue
		fi
		if [[ $line =~ ^\+?= ]]; then
			local operator="${BASH_REMATCH[0]}"
			line="${line#$operator}"
		else
			log WARN "Malformed operator: $line"
			continue
		fi
		if [[ $line =~ ^\(.*\)$ || $line =~ ^[^\(] ]]; then
			local value="$line"
		else
			log WARN "Malformed list-value: $line"
			log WARN '(expected closing round bracket at end of line)'
		fi
		_parse_config_line "$section" "$key" "$operator" "$value"
	done < <(sed "${sed_opts[@]}" "$file" | awk -v separator="$SEPARATOR" "$awt_script")
}

_parse_config_line() {
	(IFS=','; log TRACE "_parse_config_line($*)")
	# Usage: _parse_config_line SECTION KEY OPERATOR VALUE
	local section="$1"
	local key="$2"
	local operator="$3"
	local value="$4"
	local value_sep=':' set_args=() values=()
	case $operator in
	(=);; # set/replace is default
	(+=) set_args+=(--append);;
	esac
	if [[ $(type_of "$value") == 'array' ]]; then
		set_args+=(--array)
		value="${value#(}"
		value="${value%)}"
		split_string "$value_sep" "$value"
		values=("${_result[@]}")
	else
		values=("$value")
	fi
	set_config_value "${set_args[@]}" "$section" "$key" "${values[@]}"
}

set_config_value() {
	(IFS=','; log TRACE "set_config_value($*)")
	# Usage: set_config_value [MODE] [TYPE] [CONDITION] SECTION KEY [VALUE...]
	# Modes:
	#   -a|--append   Append string or array value (fail for bool)
	#   -p|--prepend  Prepend string or array value (fail for bool)
	#   -u|--unset    Unset/clear value (`-u -A` removes VALUES from array)
	#   <default>     Set/Replace string, bool or array value
	# Types:
	#   -A|--array    Ensure that the values are stored as array
	#   -b|--bool     Ensure that the values are stored as bool
	#                 (if the value is not a bool: fail)
	#   <default>     Store the values as simple string
	# Conditions:
	#   -d|--if-defined      Only perform action if value is defined
	#   -n|--if-not-defined  Only perform action if value is not defined 
	#   <default>            Always perform action
	local mode='auto'
	local type='auto'
	local cond='always'
	case $1 in
	(-a|--append) mode='append'; shift;;
	(-p|--prepend) mode='prepend'; shift;;
	(-u|--unset) mode='unset'; shift;;
	esac
	case $1 in
	(-A|--array) type='array'; shift;;
	(-b|--bool) type='bool'; shift;;
	esac
	case $1 in
	(-d|--if-defined) cond='if-defined'; shift;;
	(-n|--if-not-defined) cond='if-not-defined'; shift;;
	(-*)
		log WARN "set_config_value: unknown option: $1"
		return 1
	;;
	esac
	if [[ $# -lt 2 ]]; then
		log WARN 'set_config_value: SECTION and KEY required!'
		return 1
	fi
	local section="$1" key="$2"; shift 2
	local section_sep=':'
	local value_sep=','
	local cfg_key="$section$section_sep$key"
	local old_value="${SYSTEM_SETUP__CFG["$cfg_key"]}"
	local old_type=$(type_of "$old_value")
	local value= new_value=
	if [[ ( $cond == 'if-not-defined' && -n $old_value ) ||
	      ( $cond == 'if-defined' && -z $old_value ) ]]; then
		# condition not fulfilled
		return
	fi
	if [[ $type == 'auto' ]]; then
		if [[ $# -gt 1 ]]; then
			type='array'
		else
			type='string'
		fi
	fi
	case $type in
	(array)
		array_join "$value_sep" "$@"
		value="$_result"
	;;
	(bool)
		if _is_boolean_value "$1"; then
			value="$1"
		elif [[ -z $1 ]]; then
			log WARN "set_config_value: Bool value (true|false) required!"
			return 1
		else
			log WARN "set_config_value: Not a bool value: '$1'"
			return 1
		fi
	;;
	(string) value="$1";;
	esac
	if [[ $mode == 'auto' ]]; then
		if [[ -n $value ]]; then
			mode='set'
		else
			mode='unset'
		fi
	fi
	case $mode in
	(unset);; # nothing to do, keep empty new_value
	# TODO: --unset --array VALUES...
	(set)
		if [[ $type == 'array' ]]; then
			log TRACE '  operation: assign array'
			new_value="($value)"
		else
			log TRACE '  operation: assing string'
			new_value="$value"
		fi
	;;
	(append)
		if [[ $type == 'bool' ]] ; then
			log WARN "set_config_value: Can't append boolean config value!"
			log WARN "set_config_value:   section='$section' key='$key'"
			return 1
		fi
		if [[ $old_type == 'array' ]]; then
			log TRACE '  operation: append to array'
			new_value="${old_value%)}$value_sep$value)"
		elif [[ $type == 'array' ]]; then
			if [[ -n $old_value ]]; then
				log TRACE '  operation: convert to array + append entry'
				# note: this converts an entry to a list, even if it was first specified as string
				new_value="($old_value$value_sep$value)"
			else
				log TRACE '  operation: append to new array'
				new_value="($value)"
			fi
		else
			log TRACE '  operation: append to string'
			new_value="$old_value$value"
		fi
	;;
	(prepend)
		if [[ $type == 'bool' ]] ; then
			log WARN "set_config_value: Can't prepend boolean config value!"
			log WARN "set_config_value:   section='$section' key='$key'"
			return 1
		fi
		if [[ $old_type == 'array' ]]; then
			log TRACE '  operation: prepend to array'
			new_value="($value$value_sep${old_value#(}"
		elif [[ $type == 'array' ]]; then
			if [[ -n $old_value ]]; then
				log TRACE '  operation: convert to array + prepend entry'
				# note: this converts an entry to a list, even if it was first specified as string
				new_value="($value$value_sep$old_value)"
			else
				log TRACE '  operation: prepend to new array'
				new_value="($value)"
			fi
		else
			log TRACE '  operation: prepend to string'
			new_value="$value$old_value"
		fi
	;;
	esac
	SYSTEM_SETUP__CFG["$cfg_key"]="$new_value"
	log TRACE "  CFG[$cfg_key]: $new_value"
}

_is_array_value() {
	[[ $1 =~ ^\( && $1 =~ \)$ ]]
}

_is_boolean_value() {
	[[ -n ${1} && ( ${1,,} == 'true' || ${1,,} == 'false' ) ]]
}

type_of() {
	if [[ -z $1 ]]; then
		echo 'undefined'
	elif _is_array_value "$1"; then
		echo 'array'
	elif _is_boolean_value "$1"; then
		echo 'bool'
	else
		echo 'string'
	fi
}

get_config_value() {
	# Usage get_config_value [-a|-b] SECTION KEY [DEFAULT_VALUE...]
	local type='string'
	if [[ $1 == '-a' ]]; then
		type='array'
		shift
	elif [[ $1 == '-b' ]]; then
		type='boolean'
		shift
	fi
	local section="$1"
	local key="$2"
	shift 2
	local default_values=( "$@" )
	local section_sep=':'
	local value_sep=','
	local cfg_key="${section}${section_sep}${key}"
	local value="${SYSTEM_SETUP__CFG["$cfg_key"]}"
	if [[ $type == 'array' || $value =~ ^\( ]]; then
		value="${value#(}"
		value="${value%)}"
		unset _result
		# expand using custom separator
		local IFS="$value_sep"
		declare -ag _result=( $value )
		if [[ ${#_result[@]} == 0 ]]; then
			_result=( "${default_values[@]}" )
		fi
		log TRACE "cfg[$cfg_key] _result=(${_result[*]:+ ${_result[*]} })"
	else
		unset _result
		declare -g _result="$value"
		if [[ -z $_result ]]; then
			_result="${default_values[0]}"
		elif [[ $type == 'boolean' ]]; then
			if ! _is_boolean_value "$_result"; then
				log WARN "Expected boolean value for config key '$key' in section '$section'"
				local default_value="${default_values[0]}"
				if _is_boolean_value "$default_value"; then
					_result="$default_value"
				else
					# fall back to 'false' if no default is specified
					_result='false'
				fi
			fi
			_result="${_result,,}" # ensure lower case boolean values
		fi
		log TRACE "cfg[$cfg_key] _result='$_result'"
	fi
}

print_config_value() {
	if get_config_value "$@"; then
		if is_array _result; then
			local IFS=','
			echo "(${_result[*]})"
		else
			echo "$_result"
		fi
	fi
}

is_array() {
	local var_name="$1"
	[[ $(declare -p "$var_name") =~ ^declare\ -a ]]
}

array_contains() {
	local val match="$1"
	shift
	for val in "$@"; do
		if [[ $val == "$match" ]]; then
			return 0
		fi
	done
	return 1
}

array_copy() {
	local old="$1"
	local new="$2"
	local old_declaration=$(declare -p "$old")
	eval "$2=${old_declaration#*=}"
}

unique_array_values() {
	unset _result
	declare -ag _result=()
	local new_val known_val
	for new_val in "$@"; do
		local is_new='true'
		for known_val in "${_result[@]}"; do
			if [[ $known_val == "$new_val" ]]; then
				is_new='false'
			fi
		done
		if [[ $is_new == 'true' ]]; then
			_result+=( "$new_val" )
		fi
	done
}

array_join() {
	# Usage: array_join [-r|--prepend] DELIM [VALUE...]
	local reversed='false' value= delim=
	if [[ $1 == '-r' || $1 == '--prepend' ]]; then
		reversed='true'
		shift
	fi
	delim="$1"; shift
	unset _result; declare -g _result=
	for value in "$@"; do
		if [[ $reversed == 'true' ]]; then
			# prepend
			_result="$value${_result:+$delim$_result}"
		else
			# append
			_result="${_result:+$_result$delim}$value" 
		fi
	done
}

split_string() {
	# Usage: split_string [-r] DELIM STRING
	local reversed='false'
	if [[ $1 == '-r' ]]; then
		reversed='true'
		shift
	fi
	local delim="$1" str="$2"
	local fragment= fragments=()
	while [[ -n $str ]]; do
		if [[ $reversed == 'true' ]]; then
			# split from right to left
			fragment="${str##*$delim}"
			str="${str%$fragment}"
			str="${str%$delim*}"
			fragments+=("$fragment")
		else
			# split from left to right
			fragment="${str%%$delim*}"
			str="${str#$fragment}"
			str="${str#*$delim}"
			fragments+=("$fragment")
		fi
	done
	unset _result
	declare -ag _result=( "${fragments[@]}" )
}

_push_context() {
	# Usage: _push_context CONTEXT
	local new_context="$1"
	if [[ ${#SYSTEM_SETUP__CONTEXT_STACK[@]} == 0 ]]; then
		declare -agx SYSTEM_SETUP__CONTEXT_STACK=()
	fi
	SYSTEM_SETUP__CONTEXT_STACK=( "$new_context" "${SYSTEM_SETUP__CONTEXT_STACK[@]}" )
	declare -agx SYSTEM_SETUP__CURRENT_CONTEXT="$new_context"
}

_pop_context() {
	if [[ ${#SYSTEM_SETUP__CONTEXT_STACK[@]} -gt 0 ]]; then
		declare -agx SYSTEM_SETUP__CONTEXT_STACK=( "${SYSTEM_SETUP__CONTEXT_STACK[@]:1}" )
		declare -agx SYSTEM_SETUP__CURRENT_CONTEXT="${SYSTEM_SETUP__CONTEXT_STACK[0]}"
	fi
}

_pprint_context() {
	local context_string=''
	local separator=' > '
	for context in "${SYSTEM_SETUP__CONTEXT_STACK[@]}"; do
		if [[ -z $context_string ]]; then
			context_string="$context"
		else
			context_string="${context:+$context$separator}$context_string"
		fi
	done
	echo "$context_string"
}

is_scope_active() {
	local requested_scope="${1,,}"
	local active_scope=$(_print_active_scope)
	log TRACE "scope: req=$requested_scope, active=$active_scope"
	[[ $requested_scope == 'any' || $active_scope == 'any' || $requested_scope == "$active_scope" ]]
}

_print_active_scope() {
	if [[ -n $SYSTEM_SETUP__SCOPE ]]; then
		echo "$SYSTEM_SETUP__SCOPE"
	elif [[ $(id -u) == 0 ]]; then
		echo 'system'
	else
		echo 'user'
	fi
}

if ! (return 0 &>/dev/null); then
	log TRACE "! ${BASH_SOURCE[0]}"
else
	log TRACE ". ${BASH_SOURCE[0]}"
	if [[ -n ${_result-undefined} ]]; then
		declare -g _result=
	fi
	declare -Ag SYSTEM_SETUP__CFG=()
fi

