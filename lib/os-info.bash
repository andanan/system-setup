#!/bin/bash

os_name() {
	local name_guess=''	
	if command -v lsb_release &>/dev/null; then
		name_guess=$(lsb_release -is 2>/dev/null)
		if [[ -z $name_guess ]]; then
			local lsb_info=$(lsb_release -i 2>/dev/null)
			local lsb_info_regex='Distributor ID:[[:space:]]*([![:space:]].*)[[:space:]]*'
			if [[ $lsb_info =~ $lsb_info_regex ]]; then
				name_guess=${BASH_REMATCH[1]}
			else
				lsb_info=$(lsb_release -d 2>/dev/null)
				lsb_info_regex='Description:[[:space:]]*([![:space:]].*)[[:space:]]*'
				if [[ $lsb_info =~ $lsb_info_regex ]]; then
					name_guess=${BASH_REMATCH[1]}
				fi
			fi
		fi
	fi
	if [[ -z $name_guess ]]; then
		# analyse files in /etc
		for file in $(ls -ad1 /etc/*[-_]{release,version} 2>/dev/null); do
			if [[ -f $file ]]; then
				name_guess=$(grep -Em1 '^((DISTRIB_)?ID|NAME)=' "$file")
				name_guess="${name_guess#*=}"
				name_guess="${name_guess#'"'}"
				name_guess="${name_guess%'"'}"
				if [[ -n $name_guess ]]; then
					break
				fi
			fi
		done
		if [[ -z $name_guess ]]; then
			# last hope: generic os name
			name_guess=$(uname -s)
		fi
	fi
	name_guess="${name_guess,,}" # to lower case
	name_guess="${name_guess//[[:space:]]/}" # remove whitespace
	name_guess="${name_guess//[[:digit:]]/}" # remove numbers
	if [[ -n $name_guess ]]; then
		echo "$name_guess"
	else
		return 1
	fi
}

os_version() {
	local version_guess=''	
	if command -v lsb_release &>/dev/null; then
		version_guess=$(lsb_release -rs 2>/dev/null)
		if [[ -z $version_guess ]]; then
			local lsb_info=$(lsb_release -r 2>/dev/null)
			local lsb_info_regex='Release:[[:space:]]*([![:space:]].*)[[:space:]]*'
			if [[ $lsb_info =~ $lsb_info_regex ]]; then
				version_guess=${BASH_REMATCH[1]}
			fi
		fi
	fi
	if [[ -z $version_guess ]]; then
		# analyse files in /etc
		for file in /etc/upstream-release/lsb-release $(ls -ad1 /etc/*[-_]{release,version} 2>/dev/null); do
			if [[ -f $file ]]; then
				version_guess=$(grep -Em1 '^(DISTRIB_RELEASE|VERSION(_ID)?)=' "$file")
				version_guess="${version_guess#*=}"
				version_guess="${version_guess#'"'}"
				version_guess="${version_guess%'"'}"
				version_guess="${version_guess%%[[:space:]]*}" # extract up to first whitespace char
				if [[ -n $version_guess ]]; then
					break
				fi
			fi
		done
	fi
	if [[ -n $version_guess ]]; then
		echo "$version_guess"
	else
		return 1
	fi
}

os_codename() {
	local codename_guess=''	
	if command -v lsb_release &>/dev/null; then
		codename_guess=$(lsb_release -cs 2>/dev/null)
		if [[ -z $codename_guess ]]; then
			local lsb_info=$(lsb_release -c 2>/dev/null)
			local lsb_info_regex='Codename:[[:space:]]*([![:space:]].*)[[:space:]]*'
			if [[ $lsb_info =~ $lsb_info_regex ]]; then
				codename_guess=${BASH_REMATCH[1]}
			fi
		fi
	fi
	if [[ -z $codename_guess ]]; then
		# analyse files in /etc
		for file in $(ls -ad1 /etc/*[-_]{release,version} 2>/dev/null); do
			if [[ -f $file ]]; then
				codename_guess=$(grep -Em1 '^(((DISTRIB|VERSION)_)?CODENAME)=' "$file")
				codename_guess="${codename_guess#*=}"
				codename_guess="${codename_guess#'"'}"
				codename_guess="${codename_guess%'"'}"
				if [[ -n $codename_guess ]]; then
					break
				fi
			fi
		done
	fi
	codename_guess="${codename_guess,,}" # to lower case
	codename_guess="${codename_guess#${codename_guess%%[![:space:]]*}}" # remove leading whitespace
	codename_guess="${codename_guess%${codename_guess##*[![:space:]]}}" # remove trailing whitespace
	if [[ -n $codename_guess ]]; then
		echo "$codename_guess"
	else
		return 1
	fi
}

