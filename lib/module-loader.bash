#!/bin/bash

_print_mod_spec() {
	# ensure mod_spec. Input: module_name or mod_spec
	local mod_spec="$1"
	case $mod_spec in
		(*:*);;
		(*)
			repo=$(_print_module_repo "$mod_spec")
			if [[ -n $repo ]]; then
				mod_spec="$repo:$mod_spec"
			else
				log ERROR "Module not found: $mod_spec"
				return 1
			fi
		;;
	esac
	echo "$mod_spec"
}

_print_module_repo() {
	# Usage: _print_module_repo MOD_SPEC
	local mod_spec="$1"
	local mod_repo=''
	case $mod_spec in
	*:*) mod_repo="${mod_spec%%:*}";;
	*)
		# lookup repo name
		local mod_name="$mod_spec"
		local repos=( "${!SYSTEM_SETUP__REPOS[@]}" "$SYSTEM_SETUP__DEFAULT_REPO" )
		for repo in "${repos[@]}"; do
			if _does_module_exist "$repo:$mod_name"; then
				mod_repo="$repo"
				break
			fi
		done
	;;
	esac
	echo "$mod_repo"
}

_print_module_file() {
	# Usage: _print_module_file MOD_SPEC
	local mod_spec=$(_print_mod_spec "$1")
	local mod_repo="${mod_spec%%:*}"
	local mod_name="${mod_spec#*:}"
	if [[ -n $mod_repo && -n $mod_name ]]; then
		local repo_dir="${SYSTEM_SETUP__REPOS[$mod_repo]}"
		local modules_dir="$repo_dir"
		if [[ -d $repo_dir/modules ]]; then
			modules_dir="$repo_dir/modules"
		fi
		local mod_file="$modules_dir/$mod_name.mod.bash"
		local mod_dir_file="$modules_dir/$mod_name/module.bash"
		if [[ -f $mod_dir_file ]]; then
			echo "$mod_dir_file"
		elif [[ -f $mod_file ]]; then
			echo "$mod_file"
		fi
	fi
}

_does_module_exist() {
	# Usage: _does_module_exist MOD_SPEC
	local mod_file=$(_print_module_file "$1")
	[[ -n $mod_file && -f $mod_file ]]
}

_trigger_modules() {
	local parent_module="$1"
	local action="$2"
	if [[ ${#SYSTEM_SETUP__MODULE_TRIGGERS[@]} -gt 0 ]]; then
		local OIFS="$IFS" IFS=";"
		local triggered_modules=( ${SYSTEM_SETUP__MODULE_TRIGGERS[$parent_module]} )
		IFS="$OIFS"
		for triggered_module in "${triggered_modules[@]}"; do
			log DEBUG "Trigger module: $triggered_module"
			"$action" "$triggered_module"
		done
	fi
}

_is_module_registered() {
	array_contains "$1" "${SYSTEM_SETUP__REGISTERED_MODULES[@]}"
}

_set_module_registered() {
	local mod_spec="$1"
	if [[ ${#SYSTEM_SETUP__REGISTERED_MODULES[@]} == 0 ]]; then
		# declare new array
		declare -ag SYSTEM_SETUP__REGISTERED_MODULES=()
	fi
	unique_array_values "${SYSTEM_SETUP__REGISTERED_MODULES[@]}" "$mod_spec"
	SYSTEM_SETUP__REGISTERED_MODULES=( "${_result[@]}" )
}

_register_triggered_modules() {
	_trigger_modules "$1" 'register_module'
}

_register_mod_file() {
	local mod_spec="$1" mod_file="$2"
	if [[ ${#SYSTEM_SETUP__MODULE_FILES[@]} -eq 0 ]]; then
		declare -Agx SYSTEM_SETUP__MODULE_FILES=()
	fi
	SYSTEM_SETUP__MODULE_FILES[$mod_spec]="$mod_file"
}

register_module() {
	(IFS=',' log TRACE "register_module(${*})")
	local ec=0
	local mod_spec="$1"
	local mod_file=$(_print_module_file "$mod_spec")
	_push_context "$mod_spec"
	if [[ -n $mod_file ]]; then
		if ! _is_module_registered "$mod_spec"; then
			declare -gx SYSTEM_SETUP__CURRENT_MODULE="$mod_spec"
			log INFO "Register module: $mod_spec"
			_register_mod_file "$mod_spec" "$mod_file"
			if source "$mod_file"; then
				_set_module_registered "$mod_spec"
				_register_triggered_modules "$mod_spec"
			fi
		else
			log DEBUG "Module already registered: $mod_spec"
		fi
	else
		log WARN "Module not found: $mod_spec"
		ec=1
	fi
	# reset context
	_pop_context
	if [[ -z $SYSTEM_SETUP__CURRENT_CONTEXT ]]; then
		unset SYSTEM_SETUP__CURRENT_MODULE
	else
		declare -gx SYSTEM_SETUP__CURRENT_MODULE="$SYSTEM_SETUP__CURRENT_CONTEXT"
	fi
	return $ec
}

_is_module_initialized() {
	array_contains "$1" "${SYSTEM_SETUP__INITIALIZED_MODULES[@]}"
}

_set_module_initialized() {
	local mod_spec="$1"
	if [[ ${#SYSTEM_SETUP__INITIALIZED_MODULES[@]} == 0 ]]; then
		# declare new array
		declare -ag SYSTEM_SETUP__INITIALIZED_MODULES=()
	fi
	unique_array_values "${SYSTEM_SETUP__INITIALIZED_MODULES[@]}" "$mod_spec"
	SYSTEM_SETUP__INITIALIZED_MODULES=( "${_result[@]}" )
}

_get_module_dir_executable() {
	local mod_spec="$1"
	local mod_file="$2"
	local init_script="${SYSTEM_SETUP__INIT_SCRIPTS[$mod_spec]}"
	_result=''
	if [[ -n $init_script ]]; then
		if [[ $init_script =~ ^[^/] ]]; then
			# path relative to module dir
			init_script="${mod_file%/*}/${init_script}"
		fi
		if [[ -f $init_script ]]; then
			_result="$init_script"
		fi
	else
		_get_single_file_module_executable "$mod_spec"
	fi
}

_get_single_file_module_executable() {
	local mod_spec=$(_print_mod_spec "$1")
	local mod_name="${mod_spec#*:}"
	_result=''
	if [[ $(type -t "$mod_name") == 'function' ]]; then
		# module name = function name
		_result="$mod_name"
	fi
}

_get_module_executable() {
	local mod_spec="$1"
	local mod_file="${SYSTEM_SETUP__MODULE_FILES[$mod_spec]}"
	_result=''
	if [[ ${mod_file##*/} == 'module.bash'  ]]; then
		# module dir
		_get_module_dir_executable "$mod_spec" "$mod_file"
	else
		# single-file module
		_get_single_file_module_executable "$mod_spec"
	fi
}

_print_init_setup_script() {
	# print the initialization script that is passed as BASH_ENV
	# to the init-scripts of module-directories

	# load libraries
	local lib_files=(
		"$SYSTEM_SETUP__APP_DIR"/lib/*.bash
	)
	printf '. "%s"\n' "$SYSTEM_SETUP__APP_FILE"
	echo '_define_env_vars'
	printf '. "%s"\n' "${lib_files[@]}"

	# copy config snapshot
	echo "$(declare -p SYSTEM_SETUP__CFG)"
}

_exec_module_initializer() {
	local initializer="$1"
	local mod_spec="$2"
	local mod_file="${SYSTEM_SETUP__MODULE_FILES[$mod_spec]}"
	if [[ -f "$initializer" ]]; then
		# change context to module directory
		pushd "${mod_file%/*}" &>/dev/null
			# include lib files
			BASH_ENV=<(_print_init_setup_script) \
			"$initializer"
		popd &>/dev/null
	elif [[ $(type -t "$initializer") == 'function' ]]; then
		# only execute shell functions, ignore external commands with same name
		"$initializer"
	fi
}

_init_triggered_modules() {
	_trigger_modules "$1" 'init_module'
}

init_module() {
	(IFS=', '; log TRACE "init_module($*)")
	local mod_spec=$(_print_mod_spec "$1")
	local mod_name="${mod_spec#*:}"
	# TODO: prevent endless recursion (reuse ...CURRENT_MODULE_STACK?)
	if _is_module_registered "$mod_spec" && ! _is_module_initialized "$mod_spec"; then
		_push_context "$mod_spec"
		
		# init dependencies
		local dependency='' OIFS="$IFS" IFS=';'
		local dependencies=( ${SYSTEM_SETUP__MODULE_DEPENDENCIES[$mod_spec]} )
		IFS="$OIFS"
		for dependency in "${dependencies[@]}"; do
			init_module "$dependency"
		done

		# init module
		_get_module_executable "$mod_spec"
		local mod_exec="$_result"
		if [[ -n $mod_exec ]]; then
			log INFO "Init module: $mod_spec"
			local is_header_printed='false'
			while IFS='' read -r line || [[ -n $line ]]; do
				if [[ $is_header_printed == 'false' ]]; then
					is_header_printed='true'
					local mod_ph=$(printf '%0*d' "${#mod_name}" 0)
					mod_ph="${mod_ph//0/-}"
					echo "------------$mod_ph"
					echo "  Module: $mod_name"
					echo "------------$mod_ph"
				fi
				printf '%s\n' "$line"
			done < <(_exec_module_initializer "$mod_exec" "$mod_spec")

			if [[ $is_header_printed == 'true' ]]; then
				echo # empty line as spacer
			else
				log --no-context INFO '' # empty line as spacer
			fi
		fi

		# mark module as initialized
		_set_module_initialized "$mod_spec"

		# init triggered modules
		_init_triggered_modules "$mod_spec"

		# reset context
		_pop_context
	fi
}

_append_dependencies() {
	# Usage: _append_dependencies TARGET_MODULE  [DEPENDENCY_MODULE...]
	(IFS=', '; log TRACE "_append_dependencies($*)")
	if [[ ${#SYSTEM_SETUP__MODULE_DEPENDENCIES[@]} == 0 ]]; then
		# declare new array
		declare -Ag SYSTEM_SETUP__MODULE_DEPENDENCIES=()
	fi
	local module="$1"
	shift
	if [[ $# -gt 0 ]]; then
		local old_value="${SYSTEM_SETUP__MODULE_DEPENDENCIES["$module"]}"
		local IFS=';'
		unique_array_values $old_value "$@"
		SYSTEM_SETUP__MODULE_DEPENDENCIES["$module"]="${_result[*]}"
		log DEBUG "dependencies[$module]: ${_result[*]}"
	fi
}

mod_depends_on() {
	# Usage: mod_depends_on [--register] [MOD_SPEC...]
	(IFS=', '; log TRACE "mod_depends_on($*)")
	local append_dependencies='true'
	case $1 in
	(--register) append_dependencies='false'; shift;;
	(-*)
		log WARN "mod_depends_on: Unknown option: $1"
		return 1
	;;
	esac
	local current_module="$SYSTEM_SETUP__CURRENT_MODULE"
	local module dependencies=()
	for module in "$@"; do
		module=$(_print_mod_spec "$module")
		if ! _is_module_registered "$module"; then
			register_module "$module"
		fi
		dependencies+=("$module")
	done
	if [[ $append_dependencies == 'true' ]]; then
		_append_dependencies "$current_module" "${dependencies[@]}"
	fi
}

mod_required_by() {
	(IFS=', '; log TRACE "mod_required_by($*)")
	local current_module="$SYSTEM_SETUP__CURRENT_MODULE"
	local module
	for module in "$@"; do
		module=$(_print_mod_spec "$module")
		_append_dependencies "$module" "$current_module"
	done
}

mod_triggers() {
	(IFS=', '; log TRACE "mod_triggers($*)")
	if [[ ${#SYSTEM_SETUP__MODULE_TRIGGERS[@]} == 0 ]]; then
		# declare new array
		declare -Ag SYSTEM_SETUP__MODULE_TRIGGERS=()
	fi
	local current_module="$SYSTEM_SETUP__CURRENT_MODULE"
	local old_value="${SYSTEM_SETUP__MODULE_TRIGGERS["$current_module"]}"
	local mod mod_specs=()
	for mod in "$@"; do
		mod=$(_print_mod_spec "$mod")
		mod_specs+=( "$mod" )
	done
	local IFS=';'
	unique_array_values $old_value "${mod_specs[@]}"
	SYSTEM_SETUP__MODULE_TRIGGERS["$current_module"]="${_result[*]}"
	log DEBUG "triggers[$current_module]: ${_result[*]}"
}

mod_description() {
	local desc="$1"
	if [[ $# == 0 ]]; then
		while read -r line || [[ -n $line ]]; do
			desc+="$line$LF"
		done
	fi
	local module="$SYSTEM_SETUP__CURRENT_MODULE"
	if [[ -n $module ]]; then
		if [[ ${#SYSTEM_SETUP__MODULE_DESCRIPTIONS[@]} == 0 ]]; then
			# declare new array
			declare -Ag SYSTEM_SETUP__MODULE_DESCRIPTIONS=( ["$module"]="$desc" )
		else
			# add to existing array
			SYSTEM_SETUP__MODULE_DESCRIPTIONS+=( ["$module"]="$desc" )
		fi
	fi
}

mod_init_script() {
	local init_script="$1"
	local mod_spec="$SYSTEM_SETUP__CURRENT_MODULE"
	local mod_file="${SYSTEM_SETUP__MODULE_FILES[$mod_spec]}"
	if [[ -n ${mod_file} && ${mod_file##*/} == 'module.bash' ]]; then
		# ignore non-dir-module invocations
		if [[ ${#SYSTEM_SETUP__INIT_SCRIPTS[@]} == 0 ]]; then
			# declare new array
			declare -Ag SYSTEM_SETUP__INIT_SCRIPTS=()
		fi
		SYSTEM_SETUP__INIT_SCRIPTS["$mod_spec"]="$init_script"
	fi
}

if ! (return 0 &>/dev/null); then
	log TRACE "! ${BASH_SOURCE[0]}"
else
	log TRACE ". ${BASH_SOURCE[0]}"
fi

