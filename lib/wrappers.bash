#!/bin/bash
# wrapper functions that encapsulate external commands for debugging purposes

if ! (return 0 &>/dev/null); then
	log TRACE "! ${BASH_SOURCE[0]}"
else
	log TRACE ". ${BASH_SOURCE[0]}"
	if [[ ${SYSTEM_SETUP__ACTIVATE_WRAPPERS:-false} == 'true' ]]; then
		_exec() {
			#log INFO "$@"
			printf '!'
			local arg
			for arg in "$@"; do
				if [[ -z $arg || $arg =~ [[:space:]] ]]; then
					printf " '%s'" "$arg"
				else
					printf ' %s' "$arg"
				fi
			done
			printf \\n
		}
		git() {
			case $1 in
			symbolic-ref|for-each-ref)
				require_commands_exist 'git'
				command git "$@"
			;;
			remote)
				case $2 in
				get-url)
					require_commands_exist 'git'
					command git "$@"
				;;
				*) _exec 'git' "$@";;
				esac
			;;
			*) _exec 'git' "$@";;
			esac
		}
		flatpak() {
			case $1 in
			list|remotes)
				require_commands_exist 'flatpak'
				command flatpak "$@"
			;;
			*) _exec 'flatpak' "$@";;
			esac
		}
		make() { _exec 'make' "$@"; }
		ufw() { _exec 'ufw' "$@"; }
		add-apt-repository() { _exec 'add-apt-repository' "$@"; }
		apt-get() { _exec 'apt-get' "$@"; }
		apt-key() { _exec 'apt-key' "$@"; }
		install() { _exec 'install' "$@"; }
		hostname() { _exec 'hostname' "$@"; }
		hostnamectl() { _exec 'hostnamectl' "$@"; }
		invoke-rc.d() { _exec 'invoke-rc.d' "$@"; }
		useradd() { _exec 'useradd' "$@"; }
		usermod() { _exec 'usermod' "$@"; }
	fi
fi

