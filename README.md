# system-setup

A utility for declarative system configuration.

## Usage

Just invoke the `system-setup` command line utility to interpret and
apply the config file. (see section [Configuration](#Configuration))

## How to install

**Simple:**  
Clone the repo and symlink the `system-setup.bash` into one of your
PATH directories. (I suggest using `system-setup` as target name)

**Advanced:**  
Clone the repo and execute `make install` inside the repo directory.  
This will install the application to an appropriate location:

If executed as normal user:  
App files go into `~/.local/share/system-setup`  
The executable ends up in `~/.local/bin`

If executed as root user:  
App files end up in `/usr/local/share/system-setup`  
The executable ends up in `/usr/local/bin`

The default paths can be modified by specifying a PREFIX. 
(Take a look at the Makefile)

**Dependencies:**  
- git
- bash
- default unix tools (*)

*) if a tool is missing on your distro or some command line options are not
   supported: [open an issue](https://gitlab.com/andanan/system-setup/-/issues)

## Configuration

**system-setup** has two locations per invocation mode, where it searches
for configuration files:

**system-mode**: (root mode)

1. `/etc/system-config.cfg`
2. `/etc/system-config.d/*.cfg`

**user-mode**:

1. `~/.config/system-config.cfg`
2. `~/.config/system-config.d/*.cfg`

So for both modes, the single config file is tried first and the config directory
afterwards. The directory gives you a way to structure your config as you want.
You can put any number of config files in the config directory and they are then
loaded in directory sort order. You can therefore add a number prefix to sort
them correctly: `01_some-module.cfg`.

The loading mechanism works by overriding values defined by 'earlier' config
files. So for example if the `system-setup.cfg` file contains a line `foo=bar`
and the file `system-setup.d/01_modules.cfg` contains the line `foo=baz` then the
final value would be `foo=baz`.

### Config File Format

The config files are ini-like key-value definitions. This means that values are
defined like `some.key=value` and multiple key-value definitions can be grouped
into a section by prefixing it with a section-header like `[my-section]`.
Comment lines (lines starting with a `#` character), empty lines and leading
whitespace characters in a line are ignored.

The key-value syntax has four modes in total:

1. Simple string value definitions: `multi.part.key=Arbitrary value`
2. Append string value definitions: `multi.part.key+= more content`
3. Simple array definitions: `array.key=(value-1,value-2)`
4. Apppend array definitions: `array.key+=(value-3)`

Trailing will be spaces retained.

### Config File Example

```
# this is a comment
[system-setup]
modules=(flatpak-apt)

[system]
os=linuxmint

[apt]
packages=(gedit)

[flatpak]
packages=(com.minetest.Minetest)
```

This would set up a linux mint system and would ensure, that the installed apt
packages and the installed flatpack packages are updated, as well as install the
specified applications if they are not already installed. (flatpak and gedit with
apt and minetest with flatpak)

This works by activating the os-module `linuxmint` that has a dependency to the
`apt-pm` package that sets up and installs the configured apt packages. The
`flatpak-apt` module is a shorthand that sets up an apt dependency to the
`flatpak` package as well as a dependency to the `flatpak-pm` module. The latter
is similar to the `apt-pm` module in that it sets up and installs the configured
flatpak applications.

## Motivation

This started out as personal holiday project in december 2020. Don't expect many
updates as this is not much more as a proof-of-concept.

If you want a more sophisticated approach to declarative configuration and system
management you should take a look at other projects like [Ansible](https://www.ansible.com/),
[Puppet](https://puppet.com/open-source/) or the [Nix package manager](https://nixos.org/guides/install-nix.html).

## License

For the moment I won't put a formal license on the project. However I'm
considering to put the code into the public domain (see [unlicense.org](https://unlicense.org/))
because I frankly don't care who uses it how and for what. (Don't get me wrong,
of course I'd like you to use my software. But as this is a hobby project I'm not
very interested in getting the software out to as many people as possible)

If i didn't want anyone to use [system-setup](https://gitlab.com/andanan/system-setup),
why would I have made it available here on [GitLab.com](https://gitlab.com) in the
first place?

Ok, not a well-known license but a very simple placeholder until i decide whether
I want to put the Unlicense onto future versions of the project:

Feel free to use this software (at your own risk obviously), share it and modify it.

You owe me nothing. (You don't even have to credit me as the author!)  
I owe you nothing.  
Deal?

