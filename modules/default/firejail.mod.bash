#!/bin/bash

firejail() {
	if is_scope_active 'system'; then
		if require_commands_exist 'firejail'; then
			_configure_firejail
		fi
	else
		log WARN "firejail is only applicable for 'system' scope!"
		return 1
	fi
}

_configure_firejail() {
	: # TODO: configure firejail
}

