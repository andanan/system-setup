#!/bin/bash
display-setup-dm() {
	if require_commands_exist 'display-setup'; then
		configure_display-setup
	fi
}

configure_display-setup() {
	get_config_value -a 'display-setup-dm' 'display-configs'
	local cmd=( 'display-setup'  "${_result[@]}" )

	if is_scope_active 'system'; then
		# TODO: detect and support more display managers
		get_config_value -a 'display-setup-dm' 'integrations'
		for integration in "${_result[@]}"; do
			case $integration in
			'');;
			lightdm)
				local target_file='/etc/lightdm/lightdm.conf.d/50-display-setup.conf'
				create_file "$target_file" <<- EOF
					[SeatDefaults]
					display-setup-script=${cmd[@]}
				EOF
			;;
			*)
				log WARN "Skipping unknown integration: $integration"
			;;
			esac
		done
	else
		log WARN "display-setup-dm is only applicable for 'system' scope!"
	fi

	local do_execute=$(print_config_value -b 'display-setup-dm' 'execute' 'false')
	if [[ $do_execute == 'true' ]]; then
		exec_cmd "${cmd[@]}"
	fi
}

