#!/bin/bash

mod_depends_on --register 'apt-pm'  # only for registering
mod_required_by 'apt-pm'            # only for init

mod_description <<- EOF
	APT package manager (Ubuntu specific extensions to 'apt-pm')
EOF

### mod-lib functions

apt__add_ppa() {
	local ppa="$1"
	if [[ -n $ppa ]]; then
		if [[ $ppa != ppa:* ]]; then
			ppa="ppa:$ppa"
		fi
		apt__add_repository "$ppa" "$ppa"
	fi
}

### register-phase actions

configure_ubuntu_apt() {
	# this has to be done in register-phase because we can't add
	# repositories dynamically in the 'init'-phase.
	local ppa
	get_config_value -a 'apt' 'ppas'
	for ppa in "${_result[@]}"; do
		log INFO "Add ppa: $ppa"
		apt__add_ppa "$ppa"
	done
}

if is_scope_active 'system'; then
	require_commands_exist 'apt-get' && configure_ubuntu_apt
else
	log WARN "apt-pm-ubuntu is only applicable for 'system' scope!"
fi

