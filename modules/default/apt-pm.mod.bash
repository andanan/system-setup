#!/bin/bash

mod_description <<- EOF
	APT package manager
EOF

apt-pm() {
	local apt_cmds=('apt-get' 'apt-key' 'add-apt-repository')
	if is_scope_active 'system' && require_commands_exist "${apt_cmds[@]}"; then
		configure_apt
	else
		log WARN "apt-pm is only applicable for 'system' scope!"
		return 1
	fi
}

configure_apt() {
	# add repositories
	get_config_value -a 'apt' 'repositories'
	local exec_update='false' repos=("${_result[@]}")
	if [[ ${#repos[@]} -gt 0 ]]; then
		local repo_name clean_repo_name repo_def repo_key standalone
		for repo_name in "${repos[@]}"; do
			clean_repo_name="${repo_name//[\/:,;]/_}"  # sanitize name for use in fs-paths
			clean_repo_name="${clean_repo_name//__/_}" # reduce number of consecutive underscores
			repo_def=$(print_config_value 'apt' "repository.$repo_name")
			repo_key=$(print_config_value 'apt' "repository.$repo_name.key")
			standalone=$(print_config_value -b 'apt' "repository.$repo_name.standalone")
			if [[ -n $repo_def ]]; then
				echo "Register APT repository: $repo_name"
				# add apt repository key
				local apt_key_args=(--keyring "/etc/apt/trusted.gpg.d/$clean_repo_name.gpg")
				if [[ $repo_key == https://* ]]; then
					log WARN - <<- EOF
						It might pose a security risk to automatically download and include an unchecked
						 repository key. (URL: $repo_key)
					EOF
					exec_cmd apt-key "${apt_key_args[@]}" add - < <(download_resource "$repo_key")
				elif [[ $repo_key == /* && -f $repo_key ]]; then
					exec_cmd apt-key "${apt_key_args[@]}" add "$repo_key"
				elif [[ $repo_key = http://* ]]; then
					log WARN "Refusing to add APT repository '$repo_name': Key file must be retrieved over a secure connection!"
					continue
				elif [[ -n $repo_key ]]; then
					log WARN "Refusing to add APT repository '$repo_name': Key not found: $repo_key"
					continue
				fi
				# add repo
				if [[ $standalone == 'true' ]]; then
					! create_file "/etc/apt/sources.list.d/$clean_repo_name.list" <<- EOF
						# $repo_name
						$repo_def
					EOF
				else
					exec_cmd add-apt-repository --yes "$repo_def"
				fi
			else
				log WARN "Failed to register APT repository '$repo_name': no repo line defined!"
			fi
		done
		exec_update=true
	fi
	# update
	exec_update=$(print_config_value 'apt' 'exec.update' "$exec_update")
	local exec_upgrade=$(print_config_value 'apt' 'exec.upgrade' 'false')
	get_config_value 'apt' 'packages'
	unique_array_values "${_result[@]}"
	local packages=( "${_result[@]}" )
	if [[ ${exec_upgrade,,} == 'true' || ${#packages[@]} -gt 0 ]]; then
		# upgrade and defined packages imply update
		exec_update='true'
	fi
	if [[ $exec_update == 'true' ]]; then
		echo 'Update package cache'
		exec_cmd apt-get update
		if [[ $exec_upgrade == 'true' ]]; then
			echo 'Upgrade installed packages'
			exec_cmd apt-get upgrade --yes
		fi
		if [[ ${#packages[@]} -gt 0 ]]; then
			apt__install "${packages[@]}"
		fi
	fi
}

apt__is_pkg_installed() {
	require_commands_exist 'dpkg-query'
	dpkg-query -W --showformat='${Status}\n' "$1" |& grep -q '^install ok installed$'
}

# always executes `apt-get install`. Consider using the declarative approach via `apt__require_package` instead
apt__install() {
	log TRACE "apt__install($@)"
	require_commands_exist 'apt-get'
	local pkgs_to_install=()
	local pkg
	for pkg in "$@"; do
		if ! apt__is_pkg_installed "$pkg"; then
			pkgs_to_install+=( "$pkg" )
		else
			log DEBUG "Package already installed: $pkg"
		fi
	done
	if [[ ${#pkgs_to_install[@]} -gt 0 ]]; then
		echo 'Install APT packages:'
		printf -- '  - %s\n' "${pkgs_to_install[@]}"
		exec_cmd apt-get install --yes "${pkgs_to_install[@]}"
	fi
}

### mod-lib functions

apt__add_repository() {
	# Usage: apt__add_repository [-s|--standalone] REPO_NAME REPO_DEF [REPO_KEY]
	local standalone='false'
	if [[ $1 == '-s' || $1 == '--standalone' ]]; then
		standalone='true'
		shift
	fi
	local repo_name="$1"
	local repo_def="$2"
	local repo_key="$3"
	if [[ -n $repo_def ]]; then
		if [[ -z $(print_config_value 'apt' "repository.$repo_name.def") ]]; then
			set_config_value --append --array 'apt' 'repositories' "$repo_name"
			set_config_value 'apt' "repository.$repo_name" "$repo_def"
			set_config_value 'apt' "repository.$repo_name.key" "$repo_key"
			set_config_value 'apt' "repository.$repo_name.standalone" "$standalone"
		fi
	fi
}

apt__require_package() {
	set_config_value --append --array 'apt' 'packages' "$@"
}

