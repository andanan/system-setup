#!/bin/bash
mod_required_by 'display-setup-dm'

mod_description <<- EOF
	Install display-setup via git.
EOF

display-setup-git() {
	if ! require_commands_exist 'git'; then
		return 1
	fi
	local default_install_path='/opt/display-setup'
	local default_repo_url='https://gitlab.com/andanan/display-setup.git'
	if is_scope_active 'user'; then
		# TODO: consider ~/.local/opt as install path
		default_install_path="${XDG_DATA_HOME:-~/.local/share}/display-setup-git"
	fi
	local install_path=$(print_config_value 'display-setup-git' 'install-path' "$default_install_path")
	local repo_url=$(print_config_value 'display-setup-git' 'repo.url' "$default_repo_url")
	local repo_branch=$(print_config_value 'display-setup-git' 'repo.branch')
	git_install "$repo_url" "$install_path" ${repo_branch:+"$repo_branch"}
	exec_cmd make install -C "$install_path"
}

