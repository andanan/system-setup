#!/bin/bash

mod_description <<- EOF
	Install crypt-edit via git.
EOF

crypt-edit-git() {
	if ! require_commands_exist 'git'; then
		return 1
	fi
	local default_install_path='/opt/crypt-edit'
	local default_repo_url='https://gitlab.com/andanan/crypt-edit.git'
	if is_scope_active 'user'; then
		# TODO: consider ~/.local/opt as install path
		default_install_path="${XDG_DATA_HOME:-~/.local/share}/crypt-edit-git"
	fi
	local install_path=$(print_config_value 'crypt-edit-git' 'install-path' "$default_install_path")
	local repo_url=$(print_config_value 'crypt-edit-git' 'repo.url' "$default_repo_url")
	local repo_branch=$(print_config_value 'crypt-edit-git' 'repo.branch')
	git_install "$repo_url" "$install_path" ${repo_branch:+"$repo_branch"}
	exec_cmd make install -C "$install_path"
}

