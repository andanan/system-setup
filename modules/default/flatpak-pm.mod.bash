#!/bin/bash

mod_description <<- EOF
	Flatpak package manager
EOF

declare -ag _FLATPAK_PACKAGES=()

flatpak-pm() {
	if require_commands_exist 'flatpak'; then
		local remote remote_def pkg
		get_config_value -a 'flatpak' 'remotes'
		local remotes=( "${_result[@]}" )

		# set up remotes
		declare -A remote_defs=()
		local include_flathub=$(print_config_value 'flatpak' 'include-flathub')
		include_flathub="${include_flathub:-true}"
		if [[ ${include_flathub,,} == 'true' ]] && ! array_contains 'flathub' "${remotes[@]}"; then
			echo 'Add default flathub repository'
			remote_defs['flathub']='https://flathub.org/repo/flathub.flatpakrepo'
			remotes+=( 'flathub' )
		fi
		for remote in "${_remotes[@]}"; do
			# read remote urls
			remote_defs["$remote"]=$(print_config_value 'flatpak' "remotes.$remote")
		done
		for remote_def in "${!remote_defs[@]}"; do
			local remote_url="${remote_defs["$remote_def"]}"
			local existing_url=$(_flatpak__print_remote_url "$remote_def")
			log DEBUG "remote_def:   $remote_def"
			log DEBUG "remote_url:   $remote_url"
			log DEBUG "existing_url: $existing_url"
			if [[ -z $existing_url ]]; then
				echo "Add remote: '$remote_def' -> '$remote_url'"
				exec_cmd flatpak remote-add --if-not-exists "$remote_def" "$remote_url"
			elif [[ $existing_url == "$remote_url" ]]; then
				log INFO "Skipping existing remote: $remote_def" 
			else
				log INFO "Skipping remote: $remote_def"
				log INFO "  Configured with URL: $remote_url"
				log INFO "  But exists with URL: $existing_url"
			fi
		done

		# update applications and runtimes
		local exec_update=$(print_config_value -b 'flatpak' 'exec.update' 'true')
		if [[ ${exec_update,,} == 'true' ]]; then
			echo 'Update apps and runtimes'
			exec_cmd flatpak update --assumeyes
		fi

		# install applications
		for remote in '' "${remotes}"; do
			get_config_value -a 'flatpak' "packages${remote:+.$remote}"
			local packages=( "${_result[@]}" )
			if [[ -z $remote ]]; then
				packages+=( "${_FLATPAK_PACKAGES[@]}" )
			fi
			for pkg in "${packages[@]}"; do
				flatpak__install "$remote" "$pkg"
			done
		done

	fi
}

_flatpak__print_remote_url() {
	# Usage: flatpak__print_remote_url REMOTE_NAME
	flatpak remotes --columns name,url | grep "^$1" | cut -f2
}

_flatpak__is_pkg_installed() {
	local remote="$1"
	local pkg="$2"
	while read -r origin app || [[ -n $origin ]]; do
		if [[ ( -z $remote || $origin == "$remote" ) && $pkg == "$app" ]]; then
			return 0
		fi
	done < <(flatpak list --app --columns origin,application)
	return 1
}

# always executes `flatpak install`. Consider using the declarative approach via `flatpak__require_package` instead
flatpak__install() {
	log TRACE "flatpak__install($@)"
	if ! require_commands_exist 'flatpak'; then
		return 1
	fi
	get_config_value -a 'flatpak' 'remotes'
	local remotes=( "${_result[@]}" )
	if [[ -z $1 ]] || array_contains "$1" 'flathub' "${remotes[@]}"; then
		local remote="$1"
		shift
	else
		local remote=
	fi
	log TRACE "flatpak__install( remote='$remote', pkgs=($@) )"
	local pkgs_to_install=()
	local pkg
	for pkg in "$@"; do
		if ! _flatpak__is_pkg_installed "$remote" "$pkg"; then
			pkgs_to_install+=( "$pkg" )
		else
			log DEBUG "Package already installed: ${remote:+$remote/}$pkg"
		fi
	done
	if [[ ${#pkgs_to_install[@]} -gt 0 ]]; then
		echo "Install Flatpak packages:"
		printf '  %s\n' "${pkgs_to_install[@]}"
		local flatpak_args=( --asumeyes --noninteractive )
		if is_scope_active 'system'; then
			flatpak_args+=( --system )
		else
			flatpak_args+=( --user )
		fi
		flatpak_args+=( ${remote:+"$remote"} "${pkgs_to_install[@]}" )
		exec_cmd flatpak install "${flatpak_args[@]}"
	fi
}

flatpak__require_package() {
	local pkg
	for pkg in "$@"; do
		_FLATPAK_PACKAGES+=( "$pkg" )
	done
}
