#!/bin/bash

ufw-core() {
	if is_scope_active 'system' && require_commands_exist 'ufw'; then
		configure_ufw
	else
		log WARN "ufw is only applicable for 'system' scope!"
		return 1
	fi
}

configure_ufw() {
	get_config_value 'ufw' 'rules'
	for rule in "${_result[@]}"; do
		echo "Define rule: $rule"
		exec_cmd ufw $rule
	done

	local ufw_enabled=$(print_config_value 'ufw' 'enabled')
	ufw_enabled="${ufw_enabled:-true}"
	if [[ ${ufw_enabled,,} == 'true' ]]; then
		echo 'Enable firewall'
		exec_cmd ufw enable
	else
		echo 'Disable firewall'" $ufw_enabled"
		exec_cmd ufw disable
	fi
}

