#!/bin/bash
mod_depends_on 'os:linux' 'apt-pm'

debian() {
	if is_scope_active 'system'; then
		debian_common
		if [[ $(os_name) == 'debian' ]]; then
			debian_strict
		fi
	fi
}

debian_common() {
	exec_if scope 'system' is_active  _debian_hostname
}

_debian_is_hostname_valid() {
	local hostname_regex='^[[:alnum:]][[:alnum:].-]*[[:alnum:]]$'
	if [[ ${#new_hostname} -lt 2 || ${#new_hostname} -gt 63 ]]; then
		log WARN "Host names must have a length between 2 and 63"
		log WARN "  (actual length is ${#new_hostname})"
		return 1
	elif [[ ! $new_hostname =~ $hostname_regex ]]; then
		log WARN "Host names must match: /$hostname_regex/)"
		return 1
	fi
	return 0
}

_debian_hostname() { # TODO: consider moving this to os:linux as it already is pretty generic
	# based on: https://www.tecmint.com/set-hostname-permanently-in-linux/
	local new_hostname=$(print_config_value 'system' 'hostname')
	if [[ -z $new_hostname ]]; then
		log TRACE "Hostname module deactivated"
		log TRACE "  (config value 'system:hostname' undefined or empty)"
		return
	elif ! _debian_is_hostname_valid "$new_hostname"; then
		log WARN "Ignoring invalid hostname: $new_hostname"
		return
	fi
	local old_hostname=$(hostname -s)
	local old_file_hostname=
	if [[ -f '/etc/hostname' ]]; then
		old_file_hostname=$(cat '/etc/hostname')
	fi
	if [[ -n $old_file_hostname && $old_hostname != "$old_file_hostname" ]]; then
		# use hostname from /etc/hostname file rather than that from hostname command
		# as the latter may be changed temporarily
		old_hostname="$old_file_hostname"
		log DEBUG "Use hostname from /etc/hostname as old value: $old_hostname"
	fi
	if [[ $new_hostname == "$old_hostname" ]]; then
		log DEBUG "Hostname unchanged: $new_hostname"
		return
	fi
	echo "Update hostname: '$old_hostname' -> '$new_hostname'"
	if command -v hostnamectl &>/dev/null; then
		echo '  Update hostname via `hostnamectl` command'
		exec_cmd hostnamectl set-hostname "$new_hostname"
	elif [[ -f '/etc/hostname' ]]; then
		# exists at least on debian and fedora (TODO: verify the latter)
		echo 'Write new hostname into: /etc/hostname'
		create_file '/etc/hostname' - <<- EOF
			$new_hostname
		EOF
		local init_file
		for init_file in '/etc/init.d/hostname' '/etc/init.d/hostname.sh'; do
			if [[ -f $init_file ]]; then
				echo "  Update system state with: $init_file"
				if command -v invoke-rc.d &>/dev/null; then
					exec_cmd invoke-rc.d "$(basename "$init_file")" restart
				else
					exec_cmd "$init_file" restart
				fi
				break
			fi
		done
		if [[ $(hostname -s) != "$new_hostname" ]]; then
			echo 'Activate new hostname'
			exec_cmd hostname "$new_hostname"
		fi
	elif [[ -f '/etc/sysconfig/network' ]]; then
		# exists at least on older RHEL and CentOS versions # TODO: verify this
		sed -i "s/^\([[:space:]]*HOSTNAME=\"\)$old_hostname\(\"[[:space:]]*\)$/\1$new_hostname\2/" /etc/sysconfig/network
		# init script /etc/rc.d/rc.sysinit ?
	fi
	
	# update /etc/hosts
	echo '  Update hostname in /etc/hosts'
	local line_end_pattern="s/\([[:space:]]\)$old_hostname$/\1$new_hostname/"
	local inline_pattern="s/\([[:space:]]\)$old_hostname\([[:space:].]\)/\1$new_hostname\2/g"
	# repeat inline pattern to handle adjacent matches like: '127.0.0.1    myhost myhost.domain'
	sed -i -e "$line_end_pattern" \
	       -e "$inline_pattern" \
	       -e "$inline_pattern" \
	       /etc/hosts
}

debian_strict() {
	: # exec only on debian not on derived distros
}

