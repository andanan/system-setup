#!/bin/bash

init_file=$(readlink -f "${BASH_SOURCE[0]}")
mod_dir="${init_file%/*}"

init_system() {
	for sub_module in 'users'; do
		# subshell to prevent cluttered execution context
		(source "$mod_dir/$sub_module.bash")
	done
}

init_user() {
	: # Currently nothing to do in user scope
}

exec_if scope 'system' is_active init_system
exec_if scope 'user' is_active init_user

