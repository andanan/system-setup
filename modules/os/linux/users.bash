#!/bin/bash
# generic user setup for all linux systems

get_clean_config_value() {
	# clean config values
	get_config_value "$@"
	if [[ ${#_result[@]} -gt 0 ]]; then
		local values=()
		local value
		for value in "${_result[@]}"; do
			value="${value#${value%%[![:space:]]*}}" # cut leading whitespace
			if [[ -n $value ]]; then
				values+=("$value")
			fi
		done
		array_copy values _result
	elif ! is_array _result; then
		_result="${_result#${_result%%[![:space:]]*}}" # cut leading whitespace
	fi
}

is_setup_user_available() {
	require_commands_exist 'useradd' 'usermod'
}

is_user_existing() {
	grep "^$1:" /etc/passwd &>/dev/null
}

print_usr_file_value() {
	local user="$1"
	local column="$2"
	local file="$3"
	grep -m1 "^$user:" "$3" | cut -d: -f"$column"
}

print_passwd_value() {
	local user="$1"
	local column="$2"
	print_usr_file_value "$user" "$column" /etc/passwd
}

get_user_value() {
	local user_name="$1"
	local user_attr="$2"
	case $user_attr in
		(name) _result="$user_name";;
		(uid) _result=$(print_passwd_value "$user_name" '3');;
		(gid) _result=$(print_passwd_value "$user_name" '4');;
		(homedir) _result=$(print_passwd_value "$user_name" '6');;
		(shell) _result=$(print_passwd_value "$user_name" '7');;
		(groups)
			unset _result
			local groups=$(groups "$user_name")
			declare -ag _result=( ${groups#*:} )
		;;
	esac
}

require_shell_exists() {
	# Usage: require_shell_exists SHELL
	if [[ ! -f $1 ]]; then
		log WARN "Shell does not exist: $1"
		return 1
	elif [[ ! -x $1 ]]; then
		log WARN "Shell not executable: $1"
	fi
	return 0
}

verify_user() {
	local user_name="$1"

	echo "Verify user: $user_name"
	local usermod_args=()

	# verify uid
	local user_id=$(print_config_value 'system' "user.$user_name.uid")
	if [[ -n $user_id ]]; then
		get_user_value "$user_name" 'uid'
		local real_id="$_result"
		if [[ $user_id == "$real_id" ]]; then
			echo "  UID: $user_id -> ok"
		else
			echo "  UID mismatch: $user_id <-> $real_id"
			echo "    NOTE: Ownership of files outside of home directory must be adjusted manually"
			usermod_args+=( -u "$user_id" )
		fi
	fi

	# verify home directory
	local user_home=$(print_config_value 'system' "user.$user_name.homedir")
	if [[ -n $user_home ]]; then
		get_user_value "$user_name" 'homedir'
		local real_home="$_result"
		if [[ $user_home == "$real_home" ]]; then
			echo "  Home Dir: $user_home -> ok"
		else
			echo "  Home Dir mismatch: $user_home <-> $real_home"
			local is_user_home_present='false'
			if [[ -d $user_home && -d $real_home ]]; then
				# simply reassign home dir
				usermod_args+=( -d "$user_home" )
			elif [[ -d $real_home ]]; then
				# move old home dir
				usermod_args+=( -m -d "$user_home" )
			fi
		fi
	fi

	# verify shell
	local user_shell=$(print_config_value 'system' "user.$user_name.shell")
	if [[ -z $user_shell ]]; then
		# try default shell next
		user_shell=$(print_config_value 'system' "useradd.default.shell")
	fi
	if [[ -n $user_shell ]] && require_shell_exists "$user_shell"; then
		get_user_value "$user_name" 'shell'
		local real_shell="$_result"
		if [[ $user_shell == "$real_shell" ]]; then
			echo "  Shell: $user_shell -> ok"
		else
			echo "  Shell mismatch: $user_shell <-> $real_shell"
			usermod_args+=( -s "$user_shell" )
		fi
	fi

	# verify groups
	local user_groups=()
	get_clean_config_value -a 'system' "user.$user_name.groups"
	array_copy _result user_groups
	if [[ ${#user_groups[@]} -gt 0 ]]; then
		get_user_value "$user_name" 'groups'
		local real_groups=("${_result[@]}")
		local missing_groups=()
		local user_group assigned_group
		echo '  Groups: '
		for user_group in "${user_groups[@]}"; do
			local is_assigned='false'
			for assigned_group in "${real_groups[@]}"; do
				if [[ $user_group == "$assigned_group" ]]; then
					is_assigned='true'
				fi
			done
			if [[ $is_assigned == 'true' ]]; then
				echo "    - '$user_group' -> ok"
			else
				echo "    - '$user_group' -> assign"
				missing_groups+=("$user_group")
			fi
		done
		if [[ ${#missing_groups[@]} -gt 0 ]]; then
			local groups=$(IFS=','; echo "${missing_groups[*]}")
			usermod_args+=( -a -G "${missing_groups[@]}" )
		fi
	fi

	if [[ ${#usermod_args[@]} -gt 0 ]]; then
		echo '  Adjust user via `usermod` command'
		exec_cmd usermod "${usermod_args[@]}" "$user_name"
	fi
}

print_user_attr() {
	# Usage: print_user_attr ATTR_NAME ATTR_VALUE
	printf '  %-9s %s\n' "$1:" "$2"
}

setup_user() {
	local user_name="$1"
	local useradd_args=()

	echo "Create user: $user_name"

	# override configured default values
	for key in "${!SYSTEM_SETUP__CFG[@]}"; do
		if [[ $key =~ ^system:useradd\.key\.[^.\ \t]+$ ]]; then
			useradd_args+=(-K "${key##*.}=${SYSTEM_SETUP__CFG[$key]}")
		fi
	done

	# set uid
	local user_id=$(print_config_value 'system' "user.$user_name.uid")
	if [[ -n $user_id ]]; then
		useradd_args+=( -u "$user_id" )
		print_user_attr '  UID' "$user_id"
	fi

	# set home directory
	local user_home=$(print_config_value 'system' "user.$user_name.homedir")
	if [[ -n $user_home ]]; then
		useradd_args+=( -d "$user_home" )
		print_user_attr '  Home Dir' "$user_home"
	fi

	# set shell
	# TODO: default shell for system users: /usr/sbin/nologin
	local default_shell=$(print_config_value 'system' "useradd.default.shell")
	local user_shell=$(print_config_value 'system' "user.$user_name.shell" "$default_shell")
	if [[ -n $user_shell ]] && require_shell_exists "$user_shell"; then
		useradd_args+=( -s "$user_shell" )
		print_user_attr '  Shell' "$user_shell"
	fi

	# set groups
	local user_groups=()
	get_clean_config_value -a 'system' "user.$user_name.groups"
	array_copy _result user_groups
	if [[ ${#user_groups[@]} -gt 0 ]]; then
		local groups=$(IFS=','; echo "${user_groups[*]}")
		useradd_args+=( -G "$groups" )
		print_user_attr '  Groups' "$groups"
	fi

	if [[ ${#useradd_args[@]} -gt 0 ]]; then
		echo '  Setup user via `useradd` command'
		exec_cmd useradd "${useradd_args[@]}" "$user_name"
	fi
}

setup_users() {
	get_config_value 'system' 'users'
	local user users=( "${_result[@]}" )
	if [[ ${#users[@]} == 0 ]]; then
		return 0
	fi
	for user in "${users[@]}"; do
		if [[ -z $user ]]; then
			continue
		elif is_user_existing "$user"; then
			verify_user "$user"
		else
			setup_user "$user"
		fi
	done
}

if ! is_setup_user_available; then
	log WARN 'Could not setup system users'
	return 0
else
	setup_users
fi

## example config with default values:
# users=(<username>)
# user.<username>.homedir=<useradd-default: /home/<username>>
# user.<username>.homedir.skel=<useradd-default: /etc/skel>
# user.<username>.homedir.umask=<useradd-default: 0022>
# user.<username>.homedir.create=<useradd-default: true for normal users, false for system users>
:	# user.<username>.shell=/bin/bash
# user.<username>.disable-login=false
# user.<username>.password=<password>
:	# user.<username>.groups=()
## add no user group for system users
# user.<username>.add-usergroup=true
# user.<username>.is-admin=false
## if [ is-admin == true ]:
# user.<username>.groups+=(adm,sudo)
### Consider specifiying admin groups via dynamic config. Example:
###   in os:debian execute `set_config_value --if-not-defined 'system' 'groups.admin'  'sudo' 'adm'`
## TODO: are following groups necessary?
### (consider setting them in os-modules like: `set_config_value --if-not-defined 'system' 'groups.common' GROUPS...`)
# user.<username>.groups+=(users,cdrom,dip,plugdev,lpadmin,sambashare)
# user.<username>.groups.default=<useradd-default: <username>>
:	# user.<username>.uid=<useradd-default: next available>
## TODO: system users on debian have a uid range of (100-999) this is however not enforced by useradd by default
### This can however be specified as command line option: `-k SYS_UID_MIN=100 -k SYS_UID_MAX=999`
#### in os:debian set values like: `set_config_value 'system' 'useradd.key.SYS_UID_MIN=100'`
### TODO: check if the same applies for GID (ref: https://unix.stackexchange.com/a/121072)
# user.<username>.system-user=false
## the next 3 should all be set dynamically via os-modules if necessary
# groups.admin=()
# groups.common=()
# groups.default=()

# more infos:
# - create a home dir of user:
#	- https://ostechnix.com/create-home-directory-for-existing-user-in-linux/
# 	- `man mkhomedir_helper`
# - create user: (useradd is preferable for scripts (non-interactive usages) because it is a POSIX tool and therefore always present)
#	- `man adduser` (high level perl-script, non-standard, exists on linux and freebsd)
#		- consider configuring default adduser values like: `adduser.defaults.<key>=...`
#	- `man useradd` (low level binary, standard, should be available on all unixoid os')
#		- consider configuring default useradd values like: `useradd.defaults.<key>=...`
#	- print a message that passwords need to be changed

