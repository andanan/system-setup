#!/bin/bash
#mod_extends 'ubuntu'
# mod_extends:
#	- registers referenced modules similar to 'mod_depends_on'
#	- copy dependencies (after registration finished)
#	- copy triggers     (after registration finished)
#	- unlike depends_on, init phase does not execute init-functions of referenced modules
#	- consider simply using `mod_depends_on`
mod_depends_on 'os:ubuntu'

linuxmint() {
	if is_scope_active 'system'; then
		linuxmint__common
		if [[ $(os_name) == 'linuxmint' ]]; then
			linuxmint__strict
		fi
	fi
}

linuxmint__common() {
	local disable_sudo_pw_asterisks=$(print_config_value 'linux-mint' 'disable-sudo-pw-asterisks' 'false')
	if [[ ${disable_sudo_pw_asterisks,,} == 'true' ]]; then
		# source: https://forums.linuxmint.com/viewtopic.php?p=1572457#p1572457
	#	targetFile='/etc/sudoers.d/9_no_pwfeedback'
	#	echo 'Defaults !pwfeedback' > "$targetFile"
	#	echo "Created file: $targetFile"
	#	sed 's/^/ |/' "$targetFile"
			local target_file='/etc/sudoers.d/9_no_pwfeedback'
			create_file "$target_file" <<- EOF
				Defaults !pwfeedback
			EOF
		# The following solution would also work but won't survive an update to the `mintsystem` package.
		#mv /etc/sudoers.d/0pwfeedback /etc/sudoers.d/0pwfeedback.disabled
	else
		: # TODO: remove file if content is same
	fi
}

linuxmint__strict() {
	log DEBUG 'No strict configurations defined (yet)'
}

