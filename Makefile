ifeq (${DESTDIR}${PREFIX},)
	PREFIX = ${HOME}/.local
	ifeq (0,$(shell id -u))
		PREFIX = /usr/local
	endif
endif
app_name = system-setup
exe_name = ${app_name}
app_dir = ${DESTDIR}${PREFIX}/share/${app_name}
bin_dir = ${DESTDIR}${PREFIX}/bin

# scope
scope = $(if ${SCOPE},${SCOPE})
run-dev: override scope := $(if ${scope},${scope},any)
# log_lvl (ensure upper-case)
log_lvl := $(if ${log},$(shell echo ${log} | tr '[:lower:]' '[:upper:]'))
run-dev: override log_lvl := $(if ${log_lvl},${log_lvl},DEBUG)
# wrappers
run-dev: wrappers = true

command_env += $(if ${wrappers},SYSTEM_SETUP__ACTIVATE_WRAPPERS='${wrappers}')
command_env += $(if ${log_lvl},SYSTEM_SETUP__LOG_LVL='${log_lvl}')
command_env += $(if ${scope},SYSTEM_SETUP__SCOPE='${scope}')

all:
	@echo 'Run `make install` to install '${app_name}

install:
	@echo 'Installing ${app_name} to:'
	@echo '  app dir: ${app_dir}'
	@echo '  bin dir: ${bin_dir}'
	@mkdir -p ${app_dir} ${bin_dir}
	@cp -p -r  lib  modules  ${exe_name}.bash  ${app_dir}
	@ln -sf ${app_dir}/${exe_name}.bash ${bin_dir}/${exe_name}

uninstall:
	@echo 'Uninstalling ${app_name} from:'
	@echo '  app dir: ${app_dir}'
	@echo '  bin dir: ${bin_dir}'
	@rm ${bin_dir}/${exe_name}
	@rm -r ${app_dir}

update:
	if [ -d .git ]; then git pull; else echo "Not installed via git!" >&2; fi

run:
	${command_env} ./${app_name}.bash

run-dev: run

