#!/bin/bash

_print_log_lvl_prio() {
	local prio="${SYSTEM_SETUP__LOG_LVLS[$1]}"
	printf -- "${prio:--1}"
}

_print_color_for_log_lvl() {
	local color=''
	case $1 in
	ERROR) color='1;31';; # light-red
	WARN)  color='1;35';; # light-pink?
	INFO)  color='1;34';; # light-blue
	DEBUG) color='1;33';; # yellow
	TRACE) color='1;32';; # light-green
	esac
	if [[ -n $color ]]; then
		echo "${ESC}[${color}m"
	fi
}

log() {
	# Usage: log [OPTS...] LVL MSG [MSG...]
	#        log [OPTS...] LVL -
	local context='auto'
	local mode='echo'
	while [[ $1 == -* ]]; do
		case $1 in
			-C|--nc|--no-context) context='false'; shift;;
			-f|--format) mode='printf'; shift;;
			-*)
				log ERROR "log: Unknown option: $1"
				return 1
			;;
		esac
	done
	local msgLvl="$1"
	shift
	if [[ $msgLvl == 'OFF' ]]; then
		return 0
	fi
	local logLvl="$SYSTEM_SETUP__LOG_LVL"
	local msgPrio=$(_print_log_lvl_prio "$msgLvl")
	local logPrio=$(_print_log_lvl_prio "$logLvl")
	if [[ $context == 'auto' ]]; then
		if [[ $logPrio -ge $(_print_log_lvl_prio 'INFO') ]]; then
			# don't show context if global log level is INFO or higher
			context='false'
		else
			context='true'
		fi
	fi
	if [[ $msgPrio -ge $logPrio ]]; then
		local msg_context=''
		if command -v _pprint_context &>/dev/null && [[ $context == 'true' ]]; then
			msg_context="$(_pprint_context)"
			if [[ -n $msg_context ]]; then
				msg_context+=' | '
			fi
		fi
		if [[ $# == 0 || $1 == '-' ]]; then
			# read from stdin
			if [[ $mode == 'printf' ]]; then
				log WARN "log: Format mode not available when reading from stdin!"
			fi
			if [[ -t 2 ]]; then
				local color=$(_print_color_for_log_lvl "${msgLvl}")
				local reset="$ESC[0m"
				sed "s/\(.*\)/$color$msg_prefix\1$reset/" >&2
			else
				sed "s/^/$(printf '[%-5s] %s' "$msgLvl" "$msg_context")/" >&2
			fi
		else
			local IFS=' ' msg=''
			case $mode in
			(echo) printf -v msg '%s' "$*";;
			(printf) printf -v msg "$@";;
			esac
			msg="$msg_context$msg"
			if [[ -t 2 ]]; then
				local color=$(_print_color_for_log_lvl "${msgLvl}")
				local reset="$ESC[0m"
				printf '%s\n' "$color$msg$reset" >&2
			else
				printf '[%-5s] %s\n' "$msgLvl" "$msg" >&2
			fi
		fi
	fi
}

print_abs_path() {
	local path="$1"
	if [[ -d $path ]]; then
		(cd "$path"; pwd)
	else
		case $path in
		/*) echo "$path";;
		'~/'*) echo "$HOME${path#\~}";;
		'~'*)
			local user="${path%%/*}" home_dir=
			user="${user#\~}"
			home_dir=$(grep "^$user" /etc/passwd | cut -d: -f6)
			echo "$home_dir/${path#*/}"
		;;
		*) echo "$(pwd)/$path";;
		esac
	fi
}

_define_env_vars() {
	export TAB='	'
	export ESC=$(printf '\033')
	export LF='
'
	export SYSTEM_SETUP__APP_NAME=$(basename "$0")
	export SYSTEM_SETUP__APP_FILE=$(readlink -f "$0")
	local app_dir=$(dirname "$app_file")
	export SYSTEM_SETUP__APP_DIR=$(print_abs_path "$app_dir")
	local bin_dir=$(dirname "$0")
	export SYSTEM_SETUP__BIN_DIR=$(print_abs_path "$bin_dir")
	declare -Agx SYSTEM_SETUP__LOG_LVLS=(
		[OFF]=5
		[ERROR]=4
		[WARN]=3
		[INFO]=2
		[DEBUG]=1
		[TRACE]=0
	)
	if [[ $TRACE == 'true' ]]; then
		export SYSTEM_SETUP__LOG_LVL='TRACE'
	elif [[ $DEBUG == 'true' ]]; then
		export SYSTEM_SETUP__LOG_LVL='DEBUG'
	elif [[ -z $SYSTEM_SETUP__LOG_LVL ]]; then
		export SYSTEM_SETUP__LOG_LVL='WARN'
	elif [[ -n ${SYSTEM_SETUP__LOG_LVLS[$SYSTEM_SETUP__LOG_LVL]} ]]; then
		export SYSTEM_SETUP_LOG_LVL
	else
		log ERROR "Unknown log level: '$SYSTEM_SETUP__LOG_LVL'"
		return 1
	fi
}

_log_env_vars() {
	# this should only be called in a debugging context
	case $SYSTEM_SETUP__LOG_LVL in
	TRACE|DEBUG)
		env | grep '^SYSTEM_SETUP__' | log DEBUG -
		local level_defs=()
		for log_lvl in "${!SYSTEM_SETUP__LOG_LVLS[@]}"; do
			level_defs+=( "[$log_lvl]='${SYSTEM_SETUP__LOG_LVLS[$log_lvl]}'" )
		done
		log DEBUG "SYSTEM_SETUP__LOG_LVLS=( ${level_defs[*]} )"
	;;
	esac
}

_load_lib() {
	# Usage: _load_lib [LIB_NAME...]
	for lib_name in "$@"; do
		lib_file="${SYSTEM_SETUP__APP_DIR}/lib/$lib_name.bash"
		if [[ -f $lib_file ]]; then
			source "$lib_file"
		else
			log ERROR "It seems that your installation is broken!"
			log ERROR "Can't find file: $lib_file"
			return 1
		fi
	done
}

_load_repos() {
	local repo_dir repo_name standard_repo_base_dir
	declare -A repos=()
	standard_repo_base_dir="$1"
	for repo_dir in "$standard_repo_base_dir"/*; do
		if [[ -d $repo_dir ]]; then
			repo_name=$(basename "$repo_dir")
			repos[$repo_name]="$repo_dir"
			log DEBUG "Add standard repo '$repo_name' (dir='$repo_dir')"
		fi
	done
	get_config_value -a 'system-setup' 'repos'
	for repo_name in "${_result[@]}"; do
		local repo_ref=$(print_config_value 'system-setup' "repo.$repo_name.ref")
		if [[ -z $repo_ref ]]; then
			log WARN "Missing config: location of repo '$repo_name' (key='repo.$repo_name.ref')"
			continue
		fi
		repo_dir=
		if [[ -d $(print_abs_path "$repo_ref") ]]; then
			repo_dir=$(print_abs_path "$repo_ref")
		elif [[ $repo_ref =~ \.git$ ]]; then
			# TODO: implement dynamic git-repos
			log WARN "Could not set up repo '$repo_name': $repo_ref"
		fi
		if [[ -n $repo_dir && -d $repo_dir ]]; then
			repos[$repo_name]="$repo_dir"
			log DEBUG "Add custom repo '$repo_name' (dir='$repo_dir')"
		fi
	done
	unset _result; declare -Ag _result=()
	array_copy 'repos' '_result'
}

_build_module_list() {
	local modules=()

	# determine active os-module
	local os=$(print_config_value 'system' 'os')
	if [[ -z $os ]]; then
		# guess the current os if not defined
		os=$(os_name)
	fi
	local os_module="os:$os"
	if ! _does_module_exist "$os_module"; then
		os_module=
	fi

	# build module list
	local modules=()
	if [[ -n $os_module ]]; then
		modules+=("$os_module")
		log DEBUG "Add $(_print_module_repo "$os_module") module: $os_module"
	fi
	local repo
	for repo in "${!SYSTEM_SETUP__REPOS[@]}" ''; do
		local repo_modules=()
		get_config_value -a 'system-setup' "modules${repo:+.$repo}"
		for mod in "${_result[@]}"; do
			if [[ $mod == *:* ]]; then
				if [[ -n $repo ]]; then
					log WARN 'Repo specific modules may not contain a repo spec!'
					log WARN "(repo: '$repo'; module: '$mod')"
				else
					repo_modules+=( "$mod" )
				fi
			elif [[ -z $repo ]]; then
				# search full module spec
				mod=$(_print_mod_spec "$mod")
				repo_modules+=( "$mod" )
			else
				repo_modules+=( "$repo:$mod" )
			fi
		done
		modules+=( "${repo_modules[@]}" )
		if [[ ${#repo_modules[@]} -gt 0 ]]; then
			log DEBUG "Add ${repo:+$repo }modules: ${repo_modules[@]}"
		else
			log TRACE "No modules for repo '$repo'"
		fi
	done

	unset _result; declare -ag _result=( "${modules[@]}" )
}

_get_config_file_candidates() {
	local active_scope=$(_print_active_scope)
	local candidates=()
	case $active_scope in
	(system)
		candidates=(
			'/etc/system-setup.cfg'
			'/etc/system-setup.d/'
		)
		split_string -r ':' "${XDG_CONFIG_DIRS:-/etc/xdg}"
		local conf_dir=
		for conf_dir in "${_result[@]}"; do
			candidates+=(
				"$conf_dir/system-setup.cfg"
				"$conf_dir/system-setup.d/"
			)
		done
		candidates+=(
			${SYSTEM_SETUP_CONFIG:+"$SYSTEM_SETUP_CONFIG"}
		)
	;;
	(user|any)
		candidates=(
			"${XDG_CONFIG_HOME:-$HOME/.config}/system-setup.cfg"
			"${XDG_CONFIG_HOME:-$HOME/.config}/system-setup.d/"
			${SYSTEM_SETUP_CONFIG:+"$SYSTEM_SETUP_CONFIG"}
		)
	;;
	(*)
		log ERROR "No config file found for scope: $scope"
		return 1
	esac
	unset _result; declare -ag _result=( "${candidates[@]}" )
}

_get_config_files() {
	local candidate=
	local cfg_files=()
	_get_config_file_candidates
	for candidate in "${_result[@]}"; do
		if [[ -f $candidate ]]; then
			cfg_files+=( "$candidate" )
		elif [[ -d $candidate ]]; then
			local path=
			while IFS='' read -r path; do
				cfg_files+=( "$path" )
			done < <(find "$candidate" -maxdepth 1 -follow -type f -name '*.cfg' | sort)
		else
			log DEBUG "Not a file: $candidate"
		fi
	done
	unset _result; declare -ag _result=( "${cfg_files[@]}" )
}

_parse_config_files() {
	_get_config_files
	local cfg_file=
	for cfg_file in "${_result[@]}"; do
		log DEBUG "Parse config file: $cfg_file"
		_parse_config_file "$cfg_file"
	done
}

_main() {
	_load_lib 'core' 'module-loader' 'module-helpers' 'os-info' 'wrappers'
	_parse_config_files

	# load repos
	log DEBUG '' # empty line for spacing
	declare -gx SYSTEM_SETUP__DEFAULT_REPO='default'
	declare -Ag SYSTEM_SETUP__REPOS=()
	_load_repos "$SYSTEM_SETUP__APP_DIR/modules"
	array_copy '_result' 'SYSTEM_SETUP__REPOS'

	# build module list
	log DEBUG '' # empty line for spacing
	_build_module_list
	local modules=("${_result[@]}")

	# register and load modules
	if [[ ${#modules[@]} -gt 0 ]]; then

		# register modules
		log DEBUG '' # empty line for spacing
		for module in "${modules[@]}"; do
			! register_module "$module"
		done

		# init modules
		log INFO '' # empty line for spacing
		for module in "${modules[@]}"; do
			if ! init_module "$module"; then
				log WARN "Failed to completely initialize module: $module"
			fi
		done
	fi
}

if ! (return 0 &>/dev/null); then
	# script is being executed rather than sourced
	set -e
	_define_env_vars
	log TRACE "! $SYSTEM_SETUP__BIN_DIR/$SYSTEM_SETUP__APP_NAME ($SYSTEM_SETUP__APP_FILE)"
	_log_env_vars
	_main "$@"
else
	[[ $TRACE == 'true' ]] && log TRACE ". $0 ($(readlink -f "$0"))"
fi

